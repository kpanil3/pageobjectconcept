package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Base.BaseClass;
import Pages.AmazonHomePage;

public class AmazonHomePageTest extends BaseClass {

	@BeforeClass
	public void initialize() {
		setup();
	}

	@Test
	public void verifyMobileClick() {
		AmazonHomePage amazonHome = PageFactory.initElements(driver, AmazonHomePage.class);
		amazonHome.MobilesElementClick();
	}
	
	@AfterClass
	public void teardown() {
		shutdown();
		
	}

}
