package tests;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReporter;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import Base.BaseClass;
import Pages.AmazonHomePage;
import Pages.AmazonMobilePage;

public class AmazonMobilePageTest extends BaseClass {

	AmazonHomePage amazonHome;
	AmazonMobilePage amazonMobile;

	@BeforeClass
	public void initialize() {
		setup();

	}

	@Test
	public void searchItems() {
		amazonHome = PageFactory.initElements(driver, AmazonHomePage.class);
		amazonHome.MobilesElementClick();

	}

	@Test()
	public void getsearchedText() {
		amazonMobile = PageFactory.initElements(driver, AmazonMobilePage.class);
		amazonMobile.searchItems("Samsung");
		amazonMobile.clickSerach();
		Assert.assertEquals(amazonMobile.getSearchedText(), "\"Samsung\"");

	}

	@AfterClass
	public void teardown() {
		shutdown();

	}

}
