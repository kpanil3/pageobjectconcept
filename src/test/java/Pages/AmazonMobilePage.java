package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AmazonMobilePage {
	
	WebDriver driver ;
	public AmazonMobilePage(WebDriver driver)
	{
		this.driver = driver;
	}
	
	@FindBy(name = "field-keywords")
	private WebElement search ;
	
	@FindBy (how = How.XPATH, using = "//*[@id=\"search\"]/span/div/span/h1/div/div[1]/div/div/span[3]") 
	private WebElement SearchedText;
	
	@FindBy (how = How.ID, using = "nav-search-submit-button")
	private WebElement searchIcon ;
	
	
	
	public void searchItems(String items)
	{
		search.sendKeys(items);
	}
	
	public String getSearchedText()
	{
		return SearchedText.getText();
	}
	
	public void clickSerach()
	{
		searchIcon.click();
	}

}
